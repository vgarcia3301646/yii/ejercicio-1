<?php
    use yii\helpers\Html;
    /** @var yii\web\View $this */
    $this->title = 'Aplicación Yii - Ejercicio 1';
?>

<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Consultas ejercicio 1</h1>
    </div>

    <div class="body-content">
        <!-- Primera fila -->
        <div class="row">
            <!-- Botón de consulta -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 1</h3>
                        <p>Listar las edades de los ciclistas (sin repetidos)</p>
                        <p>
                            <?=Html::a('Active Record', ['site/consulta1ar'], ['class' => 'btn btn-primary'])?>
                            <?=Html::a('DAO', ['site/consulta1dao'], ['class' => 'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- Fin botón de consulta -->
            <!-- Botón de consulta -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 2</h3>
                        <p>Listar las edades de los ciclistas de Artiach</p>
                        <p>
                            <?=Html::a('Active Record', ['site/consulta2ar'], ['class' => 'btn btn-primary'])?>
                            <?=Html::a('DAO', ['site/consulta2dao'], ['class' => 'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- Fin botón de consulta -->
            <!-- Botón de consulta -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 3</h3>
                        <p>Listar las edades de los ciclistas de Artiach o de Amore Vita</p>
                        <p>
                            <?=Html::a('Active Record', ['site/consulta3ar'], ['class' => 'btn btn-primary'])?>
                            <?=Html::a('DAO', ['site/consulta3dao'], ['class' => 'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- Fin botón de consulta -->
        </div>
        <!-- Segunda fila -->
        <div class="row">
            <!-- Botón de consulta -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 4</h3>
                        <p>Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30</p>
                        <p>
                            <?=Html::a('Active Record', ['site/consulta4ar'], ['class' => 'btn btn-primary'])?>
                            <?=Html::a('DAO', ['site/consulta4dao'], ['class' => 'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- Fin botón de consulta -->
            <!-- Botón de consulta -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 5</h3>
                        <p>Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto</p>
                        <p>
                            <?=Html::a('Active Record', ['site/consulta5ar'], ['class' => 'btn btn-primary'])?>
                            <?=Html::a('DAO', ['site/consulta5dao'], ['class' => 'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- Fin botón de consulta -->
            <!-- Botón de consulta -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 6</h3>
                        <p>Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8</p>
                        <p>
                            <?=Html::a('Active Record', ['site/consulta6ar'], ['class' => 'btn btn-primary'])?>
                            <?=Html::a('DAO', ['site/consulta6dao'], ['class' => 'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- Fin botón de consulta -->
        </div>
        <!-- Tercera fila -->
        <div class="row">
            <!-- Botón de consulta -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 7</h3>
                        <p>Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas</p>
                        <p>
                            <?=Html::a('Active Record', ['site/consulta7ar'], ['class' => 'btn btn-primary'])?>
                            <?=Html::a('DAO', ['site/consulta7dao'], ['class' => 'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- Fin botón de consulta -->
            <!-- Botón de consulta -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 8</h3>
                        <p>Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa</p>
                        <p>
                            <?=Html::a('Active Record', ['site/consulta8ar'], ['class' => 'btn btn-primary'])?>
                            <?=Html::a('DAO', ['site/consulta8dao'], ['class' => 'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- Fin botón de consulta -->
            <!-- Botón de consulta -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 9</h3>
                        <p>Listar el nombre de los puertos cuya altura sea mayor de 1500</p>
                        <p>
                            <?=Html::a('Active Record', ['site/consulta9ar'], ['class' => 'btn btn-primary'])?>
                            <?=Html::a('DAO', ['site/consulta9dao'], ['class' => 'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- Fin botón de consulta -->
        </div>
        <!-- Cuerta fila -->
        <div class="row">
            <!-- Botón de consulta -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 10</h3>
                        <p>Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000</p>
                        <p>
                            <?=Html::a('Active Record', ['site/consulta10ar'], ['class' => 'btn btn-primary'])?>
                            <?=Html::a('DAO', ['site/consulta10dao'], ['class' => 'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- Fin botón de consulta -->
            <!-- Botón de consulta -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 11</h3>
                        <p>Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000</p>
                        <p>
                            <?=Html::a('Active Record', ['site/consulta11ar'], ['class' => 'btn btn-primary'])?>
                            <?=Html::a('DAO', ['site/consulta11dao'], ['class' => 'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- Fin botón de consulta -->
        </div>
    </div>
</div>
