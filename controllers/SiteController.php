<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\Ciclista;
use app\models\Equipo;
use app\models\Etapa;
use app\models\Lleva;
use app\models\Maillot;
use app\models\Puerto;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    public function actionConsulta1ar() {
        
        // Active Record
            // Instacia de un proveedor de datos con la consulta
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->distinct(),
        ]);
        
        // Renderizar la vista resultado
        // Parámetros para usar en la vista:
            // Proveedor de datos
            // Array de los campos que se van a visualizar en el GridView
            // Variables que se van a usar en la vista
        return $this->render('resultado',[
            'resultados' => $dataProvider,
            'campos' => ['edad'],
            'titulo' => 'Consulta 1 con Active Record',
            'enunciado' => 'Listar las edades de los ciclistas (sin repetidos)',
            'sql' => 'SELECT DISTINCT edad FROM ciclista;',
        ]);
        
    }
    
    public function actionConsulta1dao() {
        
        // DAO
            // Instacia de un proveedor de datos con la consulta
        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT DISTINCT edad FROM ciclista',
        ]);
        
        // Renderizar la vista resultado
        // Parámetros para usar en la vista:
            // Proveedor de datos
            // Array de los campos que se van a visualizar en el GridView
            // Variables que se van a usar en la vista
        return $this->render('resultado',[
            'resultados' => $dataProvider,
            'campos' => ['edad'],
            'titulo' => 'Consulta 1 con DAO',
            'enunciado' => 'Listar las edades de los ciclistas (sin repetidos)',
            'sql' => 'SELECT DISTINCT edad FROM ciclista;',
        ]);
        
    }
    
    public function actionConsulta2ar() {
        
        // Active Record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->distinct()->where("nomequipo = 'Artiach'"),
        ]);
        
        // Renderizar la vista resultado
        return $this->render('resultado',[
            'resultados' => $dataProvider,
            'campos' => ['edad'],
            'titulo' => 'Consulta 2 con Active Record',
            'enunciado' => 'Listar las edades de los ciclistas de Artiach',
            'sql' => 'SELECT DISTINCT edad FROM ciclista WHERE nomequipo = "Artiach";',
        ]);
        
    }
    
    public function actionConsulta2dao() {
        
        // DAO
        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT DISTINCT edad FROM ciclista WHERE nomequipo = "Artiach"',
        ]);
        
        // Renderizar la vista resultado
        return $this->render('resultado',[
            'resultados' => $dataProvider,
            'campos' => ['edad'],
            'titulo' => 'Consulta 2 con DAO',
            'enunciado' => 'Listar las edades de los ciclistas de Artiach',
            'sql' => 'SELECT DISTINCT edad FROM ciclista WHERE nomequipo = "Artiach";',
        ]);
        
    }
    
    public function actionConsulta3ar() {
        
        // Active Record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->distinct()->where("nomequipo = 'Artiach' or nomequipo = 'Amore Vita'"),
        ]);
        
        // Renderizar la vista resultado
        return $this->render('resultado',[
            'resultados' => $dataProvider,
            'campos' => ['edad'],
            'titulo' => 'Consulta 3 con Active Record',
            'enunciado' => 'Listar las edades de los ciclistas de Artiach o de Amore Vita',
            'sql' => 'SELECT DISTINCT edad FROM ciclista WHERE nomequipo = "Artiach" OR nomequipo = "Amore Vita";',
        ]);
        
    }
    
    public function actionConsulta3dao() {
        
        // DAO
        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT DISTINCT edad FROM ciclista WHERE nomequipo = "Artiach" OR nomequipo = "Amore Vita"',
        ]);
        
        // Renderizar la vista resultado
        return $this->render('resultado',[
            'resultados' => $dataProvider,
            'campos' => ['edad'],
            'titulo' => 'Consulta 3 con DAO',
            'enunciado' => 'Listar las edades de los ciclistas de Artiach o de Amore Vita',
            'sql' => 'SELECT DISTINCT edad FROM ciclista WHERE nomequipo = "Artiach" OR nomequipo = "Amore Vita";',
        ]);
        
    }
    
    public function actionConsulta4ar() {
        
        // Active Record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("dorsal")->distinct()->where("edad < 25 or edad > 30"),
        ]);
        
        // Renderizar la vista resultado
        return $this->render('resultado',[
            'resultados' => $dataProvider,
            'campos' => ['dorsal'],
            'titulo' => 'Consulta 4 con Active Record',
            'enunciado' => 'Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30',
            'sql' => 'SELECT DISTINCT dorsal FROM ciclista WHERE edad < 25 OR edad > 30;',
        ]);
        
    }
    
    public function actionConsulta4dao() {
        
        // DAO
        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT DISTINCT dorsal FROM ciclista WHERE edad < 25 OR edad > 30',
        ]);
        
        // Renderizar la vista resultado
        return $this->render('resultado',[
            'resultados' => $dataProvider,
            'campos' => ['dorsal'],
            'titulo' => 'Consulta 4 con DAO',
            'enunciado' => 'Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30',
            'sql' => 'SELECT DISTINCT dorsal FROM ciclista WHERE edad < 25 OR edad > 30;',
        ]);
        
    }
    
    public function actionConsulta5ar() {
        
        // Active Record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("dorsal")->distinct()->where("edad between 28 and 32 and nomequipo = 'Banesto'"),
        ]);
        
        // Renderizar la vista resultado
        return $this->render('resultado',[
            'resultados' => $dataProvider,
            'campos' => ['dorsal'],
            'titulo' => 'Consulta 5 con Active Record',
            'enunciado' => 'Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto',
            'sql' => 'SELECT DISTINCT dorsal FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo = "Banesto";',
        ]);
        
    }
    
    public function actionConsulta5dao() {
        
        // DAO
        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT DISTINCT dorsal FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo = "Banesto"',
        ]);
        
        // Renderizar la vista resultado
        return $this->render('resultado',[
            'resultados' => $dataProvider,
            'campos' => ['dorsal'],
            'titulo' => 'Consulta 5 con DAO',
            'enunciado' => 'Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto',
            'sql' => 'SELECT DISTINCT dorsal FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo = "Banesto";',
        ]);
        
    }
    
    public function actionConsulta6ar() {
        
        // Active Record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("nombre")->distinct()->where("char_length(nombre) > 8"),
        ]);
        
        // Renderizar la vista resultado
        return $this->render('resultado',[
            'resultados' => $dataProvider,
            'campos' => ['nombre'],
            'titulo' => 'Consulta 6 con Active Record',
            'enunciado' => 'Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8',
            'sql' => 'SELECT DISTINCT nombre FROM ciclista WHERE CHAR_LENGTH(nombre) > 8;',
        ]);
        
    }
    
    public function actionConsulta6dao() {
        
        // DAO
        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT DISTINCT nombre FROM ciclista WHERE CHAR_LENGTH(nombre) > 8',
        ]);
        
        // Renderizar la vista resultado
        return $this->render('resultado',[
            'resultados' => $dataProvider,
            'campos' => ['nombre'],
            'titulo' => 'Consulta 6 con DAO',
            'enunciado' => 'Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8',
            'sql' => 'SELECT DISTINCT nombre FROM ciclista WHERE CHAR_LENGTH(nombre) > 8;',
        ]);
        
    }
    
    public function actionConsulta7ar() {
        
        
        
        // Active Record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select(["nombre", "dorsal", "upper(nombre) nombre_mayúsculas"])->distinct(),
        ]);
        
        // Renderizar la vista resultado
        return $this->render('resultado',[
            'resultados' => $dataProvider,
            'campos' => ['nombre', 'dorsal', 'nombre_mayúsculas'],
            'titulo' => 'Consulta 7 con Active Record',
            'enunciado' => 'Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas',
            'sql' => 'SELECT DISTINCT nombre, dorsal, UPPER(nombre) AS "Nombre mayúsculas" FROM ciclista;',
        ]);
        
    }
    
    public function actionConsulta7dao() {
        
        // DAO
        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT DISTINCT nombre, dorsal, UPPER(nombre) AS "Nombre mayúsculas" FROM ciclista',
        ]);
        
        // Renderizar la vista resultado
        return $this->render('resultado',[
            'resultados' => $dataProvider,
            'campos' => ['nombre', 'dorsal', 'Nombre mayúsculas'],
            'titulo' => 'Consulta 7 con DAO',
            'enunciado' => 'Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas',
            'sql' => 'SELECT DISTINCT nombre, dorsal, UPPER(nombre) AS "Nombre mayúsculas" FROM ciclista;',
        ]);
        
    }
    
    public function actionConsulta8ar() {
        
        // Active Record
        $dataProvider = new ActiveDataProvider([
            'query' => Lleva::find()->select("dorsal")->distinct()->where("código = 'MGE'"),
        ]);
        
        // Renderizar la vista resultado
        return $this->render('resultado',[
            'resultados' => $dataProvider,
            'campos' => ['dorsal'],
            'titulo' => 'Consulta 8 con Active Record',
            'enunciado' => 'Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa',
            'sql' => 'SELECT DISTINCT dorsal FROM lleva WHERE código = "MGE";',
        ]);
        
    }
    
    public function actionConsulta8dao() {
        
        // DAO
        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT DISTINCT dorsal FROM lleva WHERE código = "MGE"',
        ]);
        
        // Renderizar la vista resultado
        return $this->render('resultado',[
            'resultados' => $dataProvider,
            'campos' => ['dorsal'],
            'titulo' => 'Consulta 8 con DAO',
            'enunciado' => 'Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa',
            'sql' => 'SELECT DISTINCT dorsal FROM lleva WHERE código = "MGE";',
        ]);
        
    }
    
    public function actionConsulta9ar() {
        
        // Active Record
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()->select("nompuerto")->distinct()->where("altura > 1500"),
        ]);
        
        // Renderizar la vista resultado
        return $this->render('resultado',[
            'resultados' => $dataProvider,
            'campos' => ['nompuerto'],
            'titulo' => 'Consulta 9 con Active Record',
            'enunciado' => 'Listar el nombre de los puertos cuya altura sea mayor de 1500',
            'sql' => 'SELECT DISTINCT nompuerto FROM puerto WHERE altura > 1500;',
        ]);
        
    }
    
    public function actionConsulta9dao() {
        
        // DAO
        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT DISTINCT nompuerto FROM puerto WHERE altura > 1500',
        ]);
        
        // Renderizar la vista resultado
        return $this->render('resultado',[
            'resultados' => $dataProvider,
            'campos' => ['nompuerto'],
            'titulo' => 'Consulta 9 con DAO',
            'enunciado' => 'Listar el nombre de los puertos cuya altura sea mayor de 1500',
            'sql' => 'SELECT DISTINCT nompuerto FROM puerto WHERE altura > 1500;',
        ]);
        
    }
    
    public function actionConsulta10ar() {
        
        // Active Record
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()->select("dorsal")->distinct()->where("pendiente > 8 or altura between 1800 and 3000"),
        ]);
        
        // Renderizar la vista resultado
        return $this->render('resultado',[
            'resultados' => $dataProvider,
            'campos' => ['dorsal'],
            'titulo' => 'Consulta 10 con Active Record',
            'enunciado' => 'Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000',
            'sql' => 'SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 OR altura BETWEEN 1800 AND 3000;',
        ]);
        
    }
    
    public function actionConsulta10dao() {
        
        // DAO
        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 OR altura BETWEEN 1800 AND 3000',
        ]);
        
        // Renderizar la vista resultado
        return $this->render('resultado',[
            'resultados' => $dataProvider,
            'campos' => ['dorsal'],
            'titulo' => 'Consulta 10 con DAO',
            'enunciado' => 'Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000',
            'sql' => 'SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 OR altura BETWEEN 1800 AND 3000;',
        ]);
        
    }
    
    public function actionConsulta11ar() {
        
        // Active Record
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()->select("dorsal")->distinct()->where("pendiente > 8 and altura between 1800 and 3000"),
        ]);
        
        // Renderizar la vista resultado
        return $this->render('resultado',[
            'resultados' => $dataProvider,
            'campos' => ['dorsal'],
            'titulo' => 'Consulta 11 con Active Record',
            'enunciado' => 'Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000',
            'sql' => 'SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 AND altura BETWEEN 1800 AND 3000;',
        ]);
        
    }
    
    public function actionConsulta11dao() {
        
        // DAO
        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 AND altura BETWEEN 1800 AND 3000',
        ]);
        
        // Renderizar la vista resultado
        return $this->render('resultado',[
            'resultados' => $dataProvider,
            'campos' => ['dorsal'],
            'titulo' => 'Consulta 11 con DAO',
            'enunciado' => 'Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000',
            'sql' => 'SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 AND altura BETWEEN 1800 AND 3000;',
        ]);
        
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
